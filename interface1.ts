interface Rectangle{
    width: number;
    height: number;
}

interface ColoredRestangle extends Rectangle{
    color: string
}
const rectangle:Rectangle={
    width:20,
    height:10
}

console.log(rectangle);

const coloredRestangle: ColoredRestangle={
    width:20,
    height:10,
    color:"red"
}

console.log(coloredRestangle);